package com.miknight.bameter;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class BameterActivity extends Activity {

	private TextView ptext;

	private BroadcastReceiver networkStatsReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String message = intent.getStringExtra("message");
			setMeterText(message);
		}
	};

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		Intent start = new Intent(".BameterService");
		this.startService(start);
	}

	@Override
	protected void onResume() {
		super.onResume();
		registerReceiver(networkStatsReceiver, new IntentFilter("ping-network-stats"));
	}

	@Override
	protected void onPause() {
		unregisterReceiver(networkStatsReceiver);
		super.onPause();
	}

	private void setMeterText(String s) {
		ptext = (TextView) findViewById(R.id.textView1);
		ptext.setText(s);
	}

	public void exitApplication(View view) {
		// Stop background service.
		Intent stop = new Intent(".BameterService");
		this.stopService(stop);
		// Return to the home screen.
		Intent startMain = new Intent(Intent.ACTION_MAIN);
		startMain.addCategory(Intent.CATEGORY_HOME);
		startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(startMain);
		// Destroy this activity.
		this.finish();
		android.os.Process.killProcess(android.os.Process.myPid());
	}
}
