package com.miknight.bameter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.IBinder;

public class BameterService extends Service {

	private boolean isRunning = true;
	private long downloaded = 0, uploaded = 0;
	private Thread backgroundThread = null;

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public void onCreate() {
		super.onCreate();
	}

	@Override
	// This is run every time startService() is called on it, even if it's already running.
	public int onStartCommand(Intent intent, int flags, int startId) {
		super.onStartCommand(intent, flags, startId);
		if (backgroundThread == null || !backgroundThread.isAlive()) {
			backgroundThread = new Thread(new BackgroundThread());
			backgroundThread.start();
			isRunning = true;
		}
		return START_STICKY;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		// Clear notification.
		NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		nm.cancelAll();
		isRunning = false;
	}

	private void generateNotification(String txt) {
		Resources res = getResources();
		int icon = R.drawable.notify_icon;
		String appName = res.getString(R.string.app_name);

		Context context = getApplicationContext();
		String ns = Context.NOTIFICATION_SERVICE;

		Intent notificationIntent = new Intent(context, BameterActivity.class);
		PendingIntent contentIntent = PendingIntent.getActivity(context, 1,
				notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);

		NotificationManager nm = (NotificationManager) getSystemService(ns);
		Notification notification = new Notification.Builder(context)
				.setContentTitle(appName).setContentText(txt)
				.setContentIntent(contentIntent).setOngoing(true)
				.setSmallIcon(icon).getNotification();

		nm.notify(1, notification);
	}

	private void pingNetworkStats(String device) {
		Process p;
		String downloadOutput = "";
		String uploadOutput = "";
		String debug = "";
		long newDownloaded = 0;
		long newUploaded = 0;
		try {
			p = Runtime.getRuntime().exec("cat /proc/net/dev");
			BufferedReader istream = new BufferedReader(new InputStreamReader(
					p.getInputStream()));
			String line;
			while ((line = istream.readLine()) != null) {
				debug += line + "\n";
				line = line.trim();
				// Collect I/O from all devices.
				if (line.matches("^[a-z0-9]+:.*") && !line.matches("^lo:.*")) {
					downloadOutput = line.split("\\s+")[1];
					uploadOutput = line.split("\\s+")[9];
					newDownloaded += Long.parseLong(downloadOutput);
					newUploaded += Long.parseLong(uploadOutput);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}
		String output = "";
		if (downloaded == 0) {
			downloaded = newDownloaded;
		}
		if (uploaded == 0) {
			uploaded = newUploaded;
		}
		if (downloadOutput.isEmpty() || uploadOutput.isEmpty()) {
			output = debug;
		} else {
			long d_diff = (newDownloaded - downloaded);
			long u_diff = (newUploaded - uploaded);
			output = d_diff / 1024 + " KB/s down,\n" + u_diff / 1024
					+ " KB/s up.\n";
			downloaded = newDownloaded;
			uploaded = newUploaded;
		}
		Intent intent = new Intent("ping-network-stats");
		intent.putExtra("message", output);
		sendBroadcast(intent);
		generateNotification(output);
	}

	private class BackgroundThread implements Runnable {
		public void run() {
			try {
				while (isRunning) {
					pingNetworkStats("eth0");
					Thread.sleep(1000);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
