BaMeter is an Android application that shows real-time bandwidth rates from the notification tray.

Compatible Devices
==================

Currently the app has only been tested on a Google Nexus S. If you find it works on another device, let me know and I'll update the list.

Licence
=======

This is an open source project released under the MIT licence. See the 'LICENCE' file for details.

Get In Touch
============

* _Twitter_: @miknight
* _Report a bug_: https://bitbucket.org/miknight/bameter/issues
* _Homepage_: http://bameter.miknight.com
